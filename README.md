# Getting Started

This project was developed with node v16.13.1, make sure you are running with the correct version.

Create .env at the root of your project and add the following variables :

- REACT_APP_BASE_URL=https://www.omdbapi.com/
- REACT_APP_BASE_IMAGE_URL=https://img.omdbapi.com/
- REACT_APP_API_KEY=YOUR_API_KEY

(Visit : https://www.omdbapi.com/)

Run `npm install`, then `npm start`.

# Todo
- Add default image in case a poster doesn't exist.
- Add unit tests.
- Add filters for the search.
