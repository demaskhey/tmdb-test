export const ICONS = {
    DARK_MODE: 'dark_mode',
    LIGHT_MODE: 'light_mode',
    ARROW_BACK: 'arrow_back_ios'
}

export const MESSAGES = {
    CASTING: 'Casting :',
    LOADING: 'Loading ...',
    MAX_RATE: '/ 10',
    NO_RESULT: 'No results.',
    NOT_FOUND: 'This movie has not yet been shot.',
    RATING: 'Rating :'
}

export const PLACEHOLDERS = ['Avengers...', 'Titanic...', 'Spongebob...', 'Scream...', 'Batman...', 'House of Gucci...', 'Harry Potter...']