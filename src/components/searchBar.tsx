import { useState } from "react";
import styled from "styled-components";
import { searchMovie } from "../api/moviesApi";

import useDebounce from "../utils/useDebounce";
import { PLACEHOLDERS } from "../config/AppConstant";
import { useQuery } from "react-query";
import useLocalStorage from "use-local-storage";

// Randomize the placeholder
const placeholder = PLACEHOLDERS[Math.floor(Math.random() * PLACEHOLDERS.length)];

/**
 * Prop types.
 */
type Props = {
  getResults: Function
}

const Input = styled.input`
  height: 5vh;
  font-size: large;
  margin-top: 1vh;
  border-radius: 0.5vh;
  border-style: solid;
  border-color: var(--text-primary);
  padding: 0 10px;

  @media (min-width: 1025px){
    width: 60%;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    width: 75%;
  }

  @media (min-width: 320px) and (max-width: 767px) {
    width: 95%;
  }
`

const SearchBar = ({getResults}: Props) => {
  const [searchValue, setSearchValue] = useLocalStorage("search", "");

  const [value, setValue] = useState<string>(searchValue);
  const debouncedSearchTerm = useDebounce(value, 500);

  const fetchData = async () => {
    setSearchValue(debouncedSearchTerm);
    const result = await searchMovie(debouncedSearchTerm);
    return result
  };

  const { data, status } = useQuery([`movies`, debouncedSearchTerm], fetchData);

  if (status === "success") {
    getResults(data)
  }

  //TODO: Condition should be managed upper
  return (
    <Input onChange={(e) => {setValue(e.target.value)}} onSubmit={(e) => setValue(e.currentTarget.value)} placeholder={`e.g: ${placeholder}`} value={value} />
  );
}

export default SearchBar;
